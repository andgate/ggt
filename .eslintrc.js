module.exports = {
  'env': {
    'browser': true,
    'es2021': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaVersion': 12,
    'sourceType': 'module'
  },
  'plugins': [
    '@typescript-eslint'
  ],
  'rules': {
    'indent': [
      'error',
      2
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single',
      {
        'allowTemplateLiterals': true
      }
    ],
    'semi': [
      'error',
      'never'
    ],
    'eqeqeq': 'warn',
    'curly': 'warn'
  },
  'overrides': [
    {
      'files': ['*.md'],
      'processor': 'a-plugin/markdown'
    },
    {
      'files': ['**/*.md/*.js'],
      'rules': {
        'strict': 'off'
      }
    }
  ]
}
