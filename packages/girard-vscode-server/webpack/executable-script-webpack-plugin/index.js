const fs = require('fs')

class ExecutableScriptPlugin {
  constructor(file) {
    this.file = file
  }

  apply(compiler) {
    compiler.hooks.afterEmit.tap('Executable Script Plugin', () => {
      fs.chmodSync(this.file, '755')
    })
  }
}

module.exports = ExecutableScriptPlugin
