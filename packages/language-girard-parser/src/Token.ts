
// import { string } from 'yargs'
import { Position } from './Position'
import * as P from 'parser-ts/Parser'
// import * as C from 'parser-ts/char'
// import { pipe } from 'fp-ts/pipeable'

export type Token = TokenSymbol | TokenOperator | TokenString

export type TokenSymbol = {
	_tag: 'TokenSymbol'
	pos: Position
	symbol: string
}

export type TokenOperator = {
	_tag: 'TokenOperator'
	pos: Position
	operator: string
}

export type TokenString = {
	_tag: 'TokenString'
	pos: Position
	content: string
}


export const tokensP = (): P.Parser<string, Token[]> => 
	P.many(tokenP())

const tokenP = (): P.Parser<string, Token> => undefined
	// pipe(
	// 	tokenSymbolP(),
	// 	P.either(
	// 		() => tokenOperatorP(),
	// 		() => tokenStringP()
	// 	)
	// )

// const tokenSymbolP = (): P.Parser<string, TokenSymbol> => undefined

// const tokenOperatorP = (): P.Parser<string, TokenOperator> =>
//   undefined

// const tokenStringP = (): P.Parser<string, TokenString> =>
// 	undefined
