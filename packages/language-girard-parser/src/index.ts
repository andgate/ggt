import { Either } from 'fp-ts/lib/Either'
import { Term } from 'language-girard-syntax/lib/Term'
import { termP } from './Term'
import { run } from 'parser-ts/code-frame'

// Parser has following passes
//   1. parseTokens: string -> Token[]
//   2. parseLayout: Token[] -> TokenBlock
//   3. parseStatements: TokenBlock -> Statement[]
//   4. (Token[], OpTable) -> Term

export const parse = (source: string): Either<string, Term> => run(termP(), source)
