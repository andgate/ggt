import * as babel from '@babel/core'
import GirardBabelPlugin from '../src/'

test('Plugin should run and emit expected output', function() {
  const code = 'const n = 1'

  const output = babel.transformSync(code, {
    plugins: [GirardBabelPlugin]
  })

  expect(output.code).toEqual(code)
})