const path = require('path');

module.exports = {
  entry: {
    'babel-girard': './src/index.ts',
    'babel-girard.min': './src/index.ts'
  },
  output: {
    path: path.resolve(__dirname, '_bundles'),
    filename: '[name].umd.js',
    libraryTarget: 'umd',
    library: 'LanguageGirardSyntax',
    umdNamedDefine: true
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(ts|js)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      }
    ],
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  }
};
