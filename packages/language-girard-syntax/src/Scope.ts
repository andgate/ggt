import { Term, termApp, termLam, termVar } from './Term'
import { boundVar, freeVar, Var } from './Var'

export type Scope = {
  symbol: string
  term: Term
}

export const scope = (symbol: string, term: Term): Scope => ({
  symbol,
  term: bindTerm(symbol, term)
})

export const bindTerm = (symbol: string, term: Term): Term => {
  switch(term._tag) {
    case 'TermVar':
      return termVar(bindVar(symbol, term.variable))
    case 'TermApp':
      const termA = bindTerm(symbol, term.termA)
      const termB = bindTerm(symbol, term.termB)
      return termApp(termA, termB)
    case 'TermLam':
      const x = term.scope.symbol
      const body = bindTerm(x, term.scope.term)
      return termLam(x, body)
  }
}

export const bindVar = (symbol: string, variable: Var): Var => {
  switch(variable._tag) {
    case 'BoundVar':
      return boundVar(freeVar(variable.symbol), variable.id + 1)
    case 'FreeVar':
      if (variable.symbol == symbol) {
        return boundVar(variable, 0)
      }
      return freeVar(variable.symbol)
  } 
}
